import sys
import random
import time
import argparse
import math
import csv
import os

class Simulador:
    def __init__(self, numero_simulacao, tempo_solicitacoes, capacidade, limite_tempo, atendimentos_dia, duracao_dia, desv_padrao):
        # seta os parametros iniciais para uso na simulacao
        self.numero_simulacao = numero_simulacao
        self.tempo_solicitacoes = tempo_solicitacoes
        self.limite_tempo = limite_tempo
        self.capacidade = capacidade
        self.atendimentos_dia = atendimentos_dia
        self.duracao_dia = duracao_dia
        self.desvio_padrao = desv_padrao

        #inicia variaveis de controle
        self.tempo = 0
        self.fila = []
        #define o "processador" dos atendimentos
        self.servidor = Servidor(capacidade)
        #indice de ordem de atendimento
        self.indice = 0

        self.dia = 1 # 1 = segunda 7 = Domingo
        self.hora = 0
        #array para os atendumentos finalizados
        self.finalizados = []
        


    def start(self):
        tempo_atendimento_proxima_demanda = None
        solicitacoes_geradas = 0

        desvio = random.randint(0, self.desvio_padrao)
        
        if random.randint(0, 1):
            desvio *= -1
        atendimentos_dia = self.atendimentos_dia + desvio
        print(atendimentos_dia)
        tempo_entre_demandas = self.duracao_dia // (atendimentos_dia * self.capacidade)
        while self.tempo < self.limite_tempo:
            if tempo_atendimento_proxima_demanda is None:
                tempo_atendimento_proxima_demanda = self.tempo + tempo_entre_demandas
                if self.dia >= 6:
                    tempo_atendimento_proxima_demanda = self.tempo + (tempo_entre_demandas * 2)
            if not self.tempo % self.tempo_solicitacoes: #chegou o momento de criar um novo atendimento
                novo_atendimento = Atendimento(self.tempo, self.dia, self.hora)
                self.fila.append(novo_atendimento)
                solicitacoes_geradas += 1
            
            if len(self.fila) > 0 and 8 <= self.hora <= 18:
                if tempo_atendimento_proxima_demanda <= self.tempo:
                    atendimento = self.fila[0]
                    acabou = self.servidor.processar_atendimento(atendimento, self.tempo, self.dia, self.hora)
                    self.fila = self.fila[1:]
                    if not acabou:
                        meio_fila = len(self.fila) // 2
                        self.fila.insert(meio_fila, atendimento)
                    else:
                        self.finalizados.append(atendimento)
                    tempo_atendimento_proxima_demanda = None
            self.tempo += 1

            # atualizacao do contador de dia e hora
            if self.tempo > 0 and not self.tempo % 1800: # a cada meia hora atualiza a hora.
                self.hora += 0.5
            if self.tempo > 0 and not self.tempo % 86400:
                if self.dia == 7:
                    self.dia = 1
                    self.hora = 0
                else:
                    self.dia += 1
                    self.hora = 0

                #joga a sorte do proximo dia
                desvio = random.randint(0, self.desvio_padrao)
                # joga mais um random para saber se vai atender mais ou menos
                if random.randint(0, 1):
                    desvio *= -1
                atendimentos_dia = self.atendimentos_dia + desvio
                tempo_entre_demandas = self.duracao_dia // (atendimentos_dia * self.capacidade)


        return self.fila, self.finalizados

    def tempo_atendimento_proxima_demanda(self):
        rng = 3534 #tempo médio para primeira resposta
        sd = random.randint(0, 12600)

        #tempo vai ser igual a média mais random do desvio padrão (em segundos)        

        if random.randint(0, 10) > 5:
            sd = sd * -1
        
        if sd < 0 and abs(sd) >= rng:
            return rng +1

        return rng + sd

    def exibir_parametros(self):
        print(f'\n=========== PARAMETROS DA SIMULAÇÃO {self.numero_simulacao} ===========')
        print(f'Tempo entre solicitações {self.tempo_solicitacoes}')
        print(f'Capacidade {self.capacidade}')
        print('\n')

class Atendimento:
    def __init__(self, tempo_abertura, dia, hora):
        self.tempo_abertura = tempo_abertura
        self.dia = dia
        self.hora = hora
        self.tempo_respostas = []
        
        # PROBABILIDADES NUMERO DE RESPOSTAS OBSERVAÇÕES = 809
        # 1 RESPOSTA = 696 (0,86)
        # 2 RESPOSTA = 97 (0,119)
        # 3 RESPOSTA = 13 (0,016)
        # 4 RESPOSTA = 2 (0,0024)
        # 5 RESPOSTA = 1 (0,0012)

        rng = random.uniform(0, 100)
        percentual1 = 0.86 * 100
        percentual2 = percentual1 + 0.119 * 100
        percentual3 = percentual2 + 0.016 * 100
        percentual4 = percentual3 + 0.0024 * 100

        if rng >= 0 and rng < percentual1:
            self.numero_respostas = 1
        elif rng < percentual2:
            self.numero_respostas = 2
        elif rng < percentual3:
            self.numero_respostas = 3
        elif rng < percentual4:
            self.numero_respostas = 4
        else:
            self.numero_respostas = 5
    
    def print(self):
        print(f'Número de respostas: {self.numero_respostas}')

    def responder(self, tempo, dia, hora):
        self.tempo_respostas.append({ 'tempo': tempo, 'dia': dia, 'hora': hora })
    
    @property
    def respostas_dadas(self):
        return len(self.tempo_respostas)

    def __str__(self):
        return self.tempo_abertura

class Servidor:
    def __init__(self, forca_trabalho = 1):
        self.forca_trabalho = forca_trabalho
    
    def processar_atendimento(self, atendimento, tempo, dia, hora):
        atendimento.responder(tempo, dia, hora)
        if atendimento.respostas_dadas < atendimento.numero_respostas:
            return False
        elif atendimento.respostas_dadas == atendimento.numero_respostas:
            return True

def generate_csv(n, parametros, resultado):
    fila, finalizados = resultado
    todos = fila + finalizados
    mapeados = []
    for f in todos:
        dados = {
            'tempo': f.tempo_abertura,
            'dia': f.dia,
            'hora': f.hora,
        }
        if len(f.tempo_respostas):
            resposta = f.tempo_respostas[0]
            dados['primeiraInteracao'] = resposta['tempo']
            dados['primeiraInteracaoDia'] = resposta['dia']
            dados['primeiraInteracaoHora'] = resposta['hora']

            respostaF = f.tempo_respostas[-1]
            dados['finalizacao'] = respostaF['tempo']
            dados['finalizacaoDia'] = respostaF['dia']
            dados['finalizacaoHora'] = respostaF['hora']
        else:
            dados['primeiraInteracao'] = None
            dados['primeiraInteracaoDia'] = None
            dados['primeiraInteracaoHora'] = None
            dados['finalizacao'] = None
            dados['finalizacaoDia'] = None
            dados['finalizacaoHora'] = None
        mapeados.append(dados)

        mapeados = sorted(mapeados, key=lambda x: x['tempo'])
        csv_columns = ['tempo','dia','hora']
        csv_columns += ['primeiraInteracao', 'primeiraInteracaoDia', 'primeiraInteracaoHora']
        csv_columns += ['finalizacao', 'finalizacaoDia', 'finalizacaoHora']
        csv_file = f'{n}_tempo-s-{simulador.tempo_solicitacoes}_capacidade-{simulador.capacidade}_duracao-{simulador.limite_tempo}_media-resp-dia-{simulador.atendimentos_dia}.csv'
        try:
            if not os.path.exists('resultados'):
                os.mkdir('resultados')
            with open(f'resultados/{csv_file}', 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=csv_columns, delimiter=';')
                writer.writeheader()
                for data in mapeados:
                    writer.writerow(data)
        except IOError:
            print("I/O error")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', type=int, default=3497, help='Tempo entre demandas em segundos')
    parser.add_argument('-p', type=float, default=1, help='força de trabalho')
    parser.add_argument('-l', type=int, default=604800, help='limite de tempo')
    parser.add_argument('-n', type=int, required=True, help='Número de execuções')
    parser.add_argument('-s', type=int, default=None, help='seed aleatória')
    parser.add_argument('-a', type=int, default=27, help='Atendimentos por dia')
    parser.add_argument('-d', type=int, default=28800, help='Duração dia de trabalho em segundos')
    parser.add_argument('-v', type=int, default=13, help='desvio padrão respostas por dia')
    FLAGS, unparsed = parser.parse_known_args()
    parsed_args = parser.parse_args()

    if parsed_args.s:
        random.seed(parsed_args.s)

    for i in range(0, parsed_args.n):
        simulador = Simulador(i, parsed_args.t, parsed_args.p, parsed_args.l, parsed_args.a, parsed_args.d, parsed_args.v)
        simulador.exibir_parametros()
        resultado = simulador.start()
        generate_csv(i, simulador, resultado)


    
